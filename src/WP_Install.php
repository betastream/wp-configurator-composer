<?php

namespace Betastream\WP_Configurator;

class WP_Install{

	/**
	*	called from `post-install-cmd` hook to move certain files around
	*	@param Composer\Script\Event Event
	*	@return
	*/
	public static function postUpdate( \Composer\Script\Event $event ){
		print "postUpdate called\r\n";

		$installer = new WP_Install($event->getComposer());
		$installer->cleanupWordpressFolder();
		$installer->copyWordpressConfig();
		$installer->copySampleConfig();
		return true;
	}

	function __construct($composer) {
		$config = $composer->getConfig();
		$this->vendorDir = $config->get("vendor-dir");

		$this->wordpressInstallDir = false;
		if ( $composer->getPackage() ) {
			$topExtra = $composer->getPackage()->getExtra();
			if ( ! empty( $topExtra['wordpress-install-dir'] ) ) {
				$this->wordpressInstallDir = $topExtra['wordpress-install-dir'];
			}
		}
		if ( ! $this->wordpressInstallDir ) {
			$this->wordpressInstallDir = 'wordpress';
		}

		if ( file_exists( $this->wordpressInstallDir ) && is_dir( $this->wordpressInstallDir ) ) {
			$this->wordpressInstallDir = $this->wordpressInstallDir;
		}
		else {
			echo "can't find wordpress folder";
		}
	}

	/**
	 * Makes sure some Wordpress files are removed
	 */
	public function cleanupWordpressFolder() {
		$files = [
			'license.txt', 'readme.html', 'wp-config-sample.php'
		];
		$wordpressFolder = $this->wordpressInstallDir;
		array_map( function($file_name) use($wordpressFolder) {
			$file = $wordpressFolder . '/' .$file_name;
			if( file_exists($file) ) {
				echo "removing file {$file}\r\n";
				unlink( $file );
			}
		}, $files );
	}

	/**
	 * Creates and copy the wp-config.php file to wordpress root folder
	 */
	public function copyWordpressConfig() {
		$template = __DIR__.'/wp-config.php.twig';

		$loader = new \Twig_Loader_Filesystem(dirname($template));
		$twig = new \Twig_Environment($loader);

		$templateData = array(
			'env' => $_SERVER,
			'vendor_dir' => WP_Install::getRelativePath(
				realpath($this->wordpressInstallDir),
				$this->vendorDir
			)
		);

		file_put_contents($this->wordpressInstallDir.'/wp-config.php',
			$twig->render(basename($template), $templateData)
		);

		echo "creating {$this->wordpressInstallDir}/wp-config.php\r\n";
	}

	/**
	 * Creates and copy the env-local.php file
	 */
	public function copySampleConfig() {
		$base_folder = dirname($this->wordpressInstallDir);
		if ( !file_exists($base_folder.'/env-local.php') &&
		     !file_exists($base_folder.'/env.php')) {

			$template = __DIR__.'/env-sample.php.twig';

			$loader = new \Twig_Loader_Filesystem(dirname($template));
			$twig = new \Twig_Environment($loader);

			$templateData = array(
				'env' => $_SERVER,
				'vendor_dir' => WP_Install::getRelativePath(
					realpath($this->wordpressInstallDir),
					$this->vendorDir
				)
			);

			file_put_contents($base_folder.'/env-sample.php',
				$twig->render(basename($template), $templateData)
			);

			echo "creating ".realpath($base_folder)."/env-sample.php\r\n";
			echo "rename the env-sample.php found in ".realpath($base_folder)." to env-local.php and change it\r\n";
		}
	}

	public static function getRelativePath($from, $to) {
		// some compatibility fixes for Windows paths
		$from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
		$to	 = is_dir($to)	 ? rtrim($to, '\/') . '/'	 : $to;
		$from = str_replace('\\', '/', $from);
		$to	 = str_replace('\\', '/', $to);

		$from		 = explode('/', $from);
		$to			 = explode('/', $to);
		$relPath	= $to;

		foreach($from as $depth => $dir) {
			// find first non-matching dir
			if($dir === $to[$depth]) {
				// ignore this directory
				array_shift($relPath);
			}
			else {
				// get number of remaining dirs to $from
				$remaining = count($from) - $depth;
				if($remaining > 1) {
					// add traversals up to first matching dir
					$padLength = (count($relPath) + $remaining - 1) * -1;
					$relPath = array_pad($relPath, $padLength, '..');
					break;
				}
				else {
					$relPath[0] = './' . $relPath[0];
				}
			}
		}
		return implode('/', $relPath);
	}
}
