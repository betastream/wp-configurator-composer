<?php

namespace Betastream\WP_Configurator;

/**
 * Betastream Configuration Loader
 * This class will load the environment-appropriate configuration
 * If environment is not found, it will try to get based on what exists on the filesystem
 *
 * @author Pier-Luc Lafleur <pl@beta.io>
 * @author Eric Eaglstun <eric@beta.io>
 * @since 4.2.2
 * @version 1.4.16
 */

class WP_Configurator
{

  /**
   * OpsWorks Application Environment Variable Name
   */
  const OPSWORKS_ENV_VAR    = 'APP_ENV';

  /**
   * Environment ID (dev, stg, prod)
   * @var string
   * @access public
   */
  public $env;

  /**
  * absolute path to environment config file
  */
  private $env_file;

  /**
   * absolute path to opsworks config file
   */
  private $opsworks_file;

    /**
    * Evaluates environment and load appropriate configuration file
    * If no environment variable is supplied, guess as best as we can
    * @param array $config Configuration array (from wp-config.php)
    * @return void
    * @access public
    */
    public function __construct( $config = array() )
    {
      $this->env = strtolower( getenv(self::OPSWORKS_ENV_VAR) );

      // In case we're running CLI
      if ($this->is_cli()) {
        if (!defined('WP_USE_THEMES'))
          define('WP_USE_THEMES', false);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        ini_set('display_errors', 0);
        ignore_user_abort(true);
        error_reporting(0);
      }

      // Setting constants before OpsWorks/EnvFile
      if (!empty($this->env) && !empty($config[$this->env]))
      {
        $this->loadConfigArray( $config[$this->env]  );
      }

      // Making sure that if we're behind CloudFlare's SSL, even if we're not under SSL
      // We render HTTPS links:
      if ((isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') || (isset($_SERVER['HTTP_CF_VISITOR']) && strpos($_SERVER['HTTP_CF_VISITOR'], 'https')))
        $_SERVER['HTTPS']='on';

      if (!empty($this->env))
      {
        if ($this->env == 'local')
          $this->loadFromEnvFile();
        else
          $this->loadConfigFromOpsWorks();
      }
      else
        $this->guess();

      $this->setAppEnv();
      $this->setServerEnv();

      // Are we running the admin?
      // Moved this at the end of __construct to make sure $_SERVER['HTTPS'] is set to something real
      if ((strpos($_SERVER['SERVER_NAME'], 'admin') === 0) || (strpos($_SERVER['SERVER_NAME'], 'tvo7fp5o7c5fnqntbtlqiqece') === 0))
      {
        $url = $_SERVER['HTTPS'] == 'on'? 'https://'.$_SERVER['SERVER_NAME'] : 'http://'.$_SERVER['SERVER_NAME'];
        define(WP_SITEURL, $url);
        define(WP_HOME,    $url);
      }

    }

  /**
   * Try to guess the environment
   * @param null
   * @return void
   * @access private
   */
  private function guess()
  {
    if ($this->is_env() )
      return $this->loadFromEnvFile();
    // This was originally an elseif (and is_opsworks was first)
    // But I've elected to support both methods and favour OpsWorks.
    // -@pluc
    if ($this->is_opsworks())
      return $this->loadConfigFromOpsWorks();

    // Nothing to do
    trigger_error( 'Unknown environment', E_USER_ERROR );
  }

  /**
   * Are we running CLI?
   * Checks php_sapi
   * @param null
   * @return bool
   * @access public
   */
  public function is_cli()
  {
    return (php_sapi_name() == 'cli');
  }

  /**
   * Are we running OpsWorks?
   * Checks if opsworks.php exists
   * @param null
   * @return bool
   * @access public
   */
  public function is_opsworks()
  {
    $path = __DIR__;
    $pos = $pos=strpos($path, '/releases');
    if ($pos !== FALSE) {
      $path = substr($path, 0, $pos+24);
      if (file_exists($path.'/opsworks.php')) {
        $this->opsworks_file = $path.'/opsworks.php';
        return true;
      }
    }

    return false;
  }

  /**
   * Loads OpsWorks Configuration
   * @param null
   * @return bool
   * @access public
   */
  public function loadConfigFromOpsWorks()
  {
    if ($this->is_opsworks())
    {
      include $this->opsworks_file;

      if (class_exists('\\OpsWorks'))
      {
        $opsworks = new \OpsWorks();
        define('DB_CHARSET',  $opsworks->db->encoding);
        define('DB_HOST',     $opsworks->db->host);
        define('DB_NAME',     $opsworks->db->database);
        define('DB_PASSWORD', $opsworks->db->password);
        define('DB_USER',     $opsworks->db->username);

        switch ( $this->env )
        {
          case 'prod':
          case 'uat':
            error_reporting( 0 );
            ini_set('display_errors',             FALSE);

            $constants = array(
              'SCRIPT_DEBUG' => FALSE,
              'WP_DEBUG' => FALSE,
              'WP_DEBUG_DISPLAY' => FALSE,
              'WP_DEBUG_LOG' => FALSE,
              'WCS_DEBUG' => FALSE,
              'DISALLOW_FILE_MODS' => TRUE,
              'DISALLOW_FILE_EDIT' => TRUE,
              'DISABLE_WP_CRON' => defined('DISABLE_WP_CRON')? DISABLE_WP_CRON : TRUE,
              'AUTOMATIC_UPDATER_DISABLED' => defined('AUTOMATIC_UPDATER_DISABLED')? AUTOMATIC_UPDATER_DISABLED : FALSE
            );
          break;

          case 'stg':
            error_reporting( E_ALL | E_STRICT );
            ini_set('display_errors',             TRUE);

            $constants = array(
              'SCRIPT_DEBUG' => TRUE,
              'WP_DEBUG' => TRUE,
              'WP_DEBUG_LOG' => TRUE,
              'WP_DEBUG_DISPLAY' => TRUE,
              'WCS_DEBUG' => TRUE,
              'DISALLOW_FILE_MODS' => TRUE,
              'DISALLOW_FILE_EDIT' => TRUE,
              'DISABLE_WP_CRON' => defined('DISABLE_WP_CRON')? DISABLE_WP_CRON : TRUE,
              'AUTOMATIC_UPDATER_DISABLED' => defined('AUTOMATIC_UPDATER_DISABLED')? AUTOMATIC_UPDATER_DISABLED : FALSE
            );
          break;

          case 'dev':
            error_reporting( E_ALL );
            ini_set('display_errors',             TRUE);

            $constants = array(
              'SCRIPT_DEBUG' =>               TRUE,
              'WP_DEBUG' =>                   TRUE,
              'WP_DEBUG_DISPLAY' =>           TRUE,
              'WCS_DEBUG' =>                  TRUE,
              'AUTOMATIC_UPDATER_DISABLED' => TRUE
            );
            break;

          default:
            $msg = sprintf( "No Environment Found! \r\nEnv: %s\r\nStack: %s", $this->env, $opsworks->stack_name );
            error_log( $msg, 0 );
            //die( $msg );
            break;
        }

        foreach( $constants as $k=>$v ){
          if( !defined($k) )
            define( $k, $v );
        }

        return true;
      }
      else trigger_error("Error loading configuration (".__LINE__.")");
  }
    return false;
  }

  /**
   * Are we running with the legac env-*.php files?
   * @param null
   * @return bool
   * @access public
   */
  public function is_env()
  {
    if (!defined('ABSPATH')) {
      die('ABSPATH not defined. Are you sure we are running Wordpress?');
    }

    // ordered best to worst
    $paths = array(
      dirname(ABSPATH).'/',
      dirname(ABSPATH).'/__wp_config/',
      ABSPATH
    );

    $files = array(
      'env-local.php',
      'env.php'
    );

    foreach( $paths as $path ){
      foreach( $files as $file_name ){
        $file = $path.$file_name;
        // error_log("checking file: {$file}");
        if( file_exists($file) ){
          $this->env_file = $file;
         return true;
        }
      }
    }

    return false;
  }

  /**
   *
   *  @param array
   */
  private function loadConfigArray( $config ){
    if( !empty($config['constants']) )
      foreach ($config['constants'] as $k=>$v)
        define($k, $v);

    if( !empty($config['globals']) )
      foreach ($config['globals'] as $k=>$v)
        $GLOBALS[$k] = $v;
  }

  /**
   * Loads configuration from enviroment .php file
   * @param null
   * @return void
   * @access private
   */
   private function loadFromEnvFile()
   {
     //if (empty($this->env))
     // $this->env='local';

     $config = require $this->env_file;

     if( is_array($config) )
      $this->loadConfigArray( $config );
   }

  /**
   * Sets Environment Across the Board (ENV + CONST)
   * @param null
   * @return void
   * @access private
   */
  private function setAppEnv()
  {
    if (!empty($this->env))
    {
      $env = strtoupper($this->env);
      putenv("APP_ENV=$env");
      define('APP_ENV', $env);
    }
  }

  /**
  * loads environment variables from AWS
  * @return void
  * @access private
  */
  private function setServerEnv(){
    $vars = array(
      # Application Environment (DEV, STG, UAT, PROD)
      'APP_ENV',
      # S3 Buckets for use with the S3 Offload Plugin
      'AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY',
      # Whether to force SSL (SJR-1621)
      'FORCE_SSL',
      # WordPress Salts (SYS-121)
      'AUTH_KEY', 'SECURE_AUTH_KEY', 'LOGGED_IN_KEY',
      'NONCE_KEY', 'AUTH_SALT', 'SECURE_AUTH_SALT',
      'LOGGED_IN_SALT', 'NONCE_SALT',
    );
    foreach( $vars as $k ){
      if( !defined($k) ){
        $v = getenv( $k );

        if( $v !== FALSE ){
          putenv( "$k=$v" );
          define( $k, $v );
        }
      }
    }
  }
}
